/**************************************************
*** zws
*** 20220224
**************************************************/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define PTHREAD_NUM 3

#define LL_ADD(item, list)                                                     \
  do {                                                                         \
    item->prev = NULL;                                                         \
    item->next = list;                                                         \
    if (list != NULL)                                                          \
      list->prev = item;                                                       \
    list = item;                                                               \
  } while (0)

#define LL_REMOVE(item, list)                                                  \
  do {                                                                         \
    if (item->prev != NULL)                                                    \
      item->prev->next = item->next;                                           \
    if (item->next != NULL)                                                    \
      item->next->prev = item->prev;                                           \
    if (list == item)                                                          \
      list = item->next;                                                       \
    item->prev = item->next = NULL;                                            \
  } while (0)

//
struct ZWORKER {
  pthread_t thread;
  struct ZMANAGER *pool;

  //
  volatile int terminate;

  volatile int is_online;

  //
  pthread_cond_t tasks_cond;
  //
  pthread_mutex_t tasks_mutex;

  struct ZWORKER *prev;
  struct ZWORKER *next;
};

//
struct ZTASK {
  void (*func)(void *);
  void *user_data;
  volatile int is_running;

  struct ZTASK *prev;
  struct ZTASK *next;
};

//
struct ZMANAGER {
  //
  struct ZWORKER *workers;

  //
  struct ZTASK *tasks;

  uint thread_number;

  pthread_cond_t pool_cond;
  pthread_mutex_t pool_mutex;
};

typedef struct ZMANAGER nThreadPool;

//
static void *nThreadCallback(void *arg) {
  printf("---------------------nThreadCallback entry---------------thread "
         "ID:%ld\n",
         pthread_self());
  struct ZWORKER *worker = (struct ZWORKER *)arg;

  while (1) {
    pthread_mutex_lock(&worker->tasks_mutex);

    pthread_cond_wait(&worker->tasks_cond, &worker->tasks_mutex);

    if (worker->terminate) {
      pthread_mutex_unlock(&worker->tasks_mutex);
      break;
    }
    worker->is_online = 1;

    struct ZTASK *task = worker->pool->tasks;
    task->is_running = 1;
    LL_REMOVE(task, worker->pool->tasks);
    pthread_mutex_unlock(&worker->tasks_mutex);

    task->func(task->user_data);

    pthread_mutex_lock(&worker->tasks_mutex);
    worker->is_online = 0;
    pthread_mutex_unlock(&worker->tasks_mutex);
  }

  // free(worker);
  printf(
      "---------------------nThreadCallback exit---------------thread ID:%ld\n",
      pthread_self());
  // pthread_exit(NULL);
  return NULL;
}

//
int nThreadPoolCreate(nThreadPool *pool, int numWorkers) {
  int i = 0;

  if (numWorkers < 1)
    numWorkers = 1;

  if (pool == NULL)
    return -1;
  memset(pool, 0, sizeof(nThreadPool));
  pool->thread_number = numWorkers;
  pthread_mutex_init(&pool->pool_mutex, NULL);
  pthread_cond_init(&pool->pool_cond, NULL);

  for (i = 0; i < numWorkers; i++) {

    struct ZWORKER *worker = (struct ZWORKER *)malloc(sizeof(struct ZWORKER));

    if (worker == NULL) {
      perror("malloc");
      return -2;
    }

    memset(worker, 0, sizeof(struct ZWORKER));
    worker->pool = pool;
    worker->is_online = 0;

    int ret = pthread_create(&worker->thread, NULL, nThreadCallback, worker);
    if (ret) {
      perror("pthread_create");
      return -3;
    }

    pthread_mutex_init(&worker->tasks_mutex, NULL);
    pthread_cond_init(&worker->tasks_cond, NULL);

    LL_ADD(worker, pool->workers);
  }
  printf(
      "---------------------nThreadPoolCreate success!---------------------\n");
  return 0;
}

void nThreadPoolDestroy(nThreadPool *pool) {
  struct ZWORKER *worker = NULL;
  int is_online = 0;

  while (1) {
    is_online = 0;
    sleep(1);
    worker = pool->workers;
    while (worker) {
      is_online += worker->is_online;
      worker = worker->next;
    }
    if (!is_online)
      break;
  }

  for (worker = pool->workers; worker != NULL; worker = worker->next) {
    pthread_mutex_lock(&worker->tasks_mutex);
    worker->terminate = 1;
    pthread_mutex_unlock(&worker->tasks_mutex);
    pthread_cond_signal(&worker->tasks_cond);
  }

  for (worker = pool->workers; worker != NULL; worker = worker->next) {
    pthread_join(worker->thread, NULL);
  }

  worker = pool->workers;
  while (worker->next) {
    worker = worker->next;
    free(worker->prev);
  }
  free(worker);
}

void nThreadPoolPush(nThreadPool *pool, struct ZTASK *task) {
  struct ZWORKER *worker = NULL;
  while (1) {
    for (worker = pool->workers; worker != NULL; worker = worker->next) {
      if (worker->is_online == 0)
        break;
    }
    if (worker)
      break;
  }
  pthread_mutex_lock(&worker->tasks_mutex);
  LL_ADD(task, pool->tasks);
  pthread_mutex_unlock(&worker->tasks_mutex);

  while (!worker->is_online) {
    pthread_cond_signal(&worker->tasks_cond);
  }

  // pthread_mutex_unlock(&worker->tasks_mutex);
}

#if 1

//
static void my_taskFunc(void *user_data) {
  if (user_data == NULL)
    return;
  printf("thread ID:%ld, data:%d\n", pthread_self(), *(int *)user_data);
  sleep(1);
}

int main() {
  int i = 0;
  struct ZTASK *task_node = NULL;

  nThreadPool *pool = (nThreadPool *)malloc(sizeof(struct ZMANAGER));

  int ret = nThreadPoolCreate(pool, PTHREAD_NUM);

  if (ret != 0) {
    return -1;
  }
  printf("-----------------Main thread ID:%ld----------------\n",
         pthread_self());

  task_node = (struct ZTASK *)malloc(10 * sizeof(struct ZTASK));
  if (task_node == NULL) {
    perror("malloc task_node");
    return -2;
  }
  for (i = 0; i < 10; i++) {
    (task_node + i)->func = my_taskFunc;
    (task_node + i)->user_data = (void *)malloc(sizeof(int));
    memcpy((task_node + i)->user_data, &i, sizeof(int));
  }

  for (i = 0; i < 10; i++) {
    nThreadPoolPush(pool, task_node + i);
  }

  // while (pool->tasks != NULL) {
  //   sleep(1);
  // }

  nThreadPoolDestroy(pool);
  free(pool);

  for (i = 0; i < 10; i++) {
    free((task_node + i)->user_data);
  }
  free(task_node);
  printf("-----------------Main thread Exit----------------\n");
  return 0;
}

#endif
