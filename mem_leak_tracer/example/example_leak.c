#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void func()
{
  char *space = (char *)calloc(1, 101);
  memcpy(space, "world", 5);
  fprintf(stderr, "%s\n", (char*)space);
}

int main()
{
    char *space = (char *)malloc(100);
    memcpy(space, "hello", 5);
    fprintf(stderr, "%s\n", (char*)space);
    printf("hello world\n");
    //free(space);
    func();
    return 0;
}
