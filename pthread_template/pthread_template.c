#include <pthread.h>
#include <stdio.h>

static pthread_mutex_t m1, m2;
static pthread_cond_t cond1, cond2;
static int count = 0;
static volatile int is_running1 = 0;
static volatile int is_running2 = 0;
static volatile int is_running = 0;

void *first(void *a)
{
  for (int x = 0; x < 10; x++) {

    printf("count: %d\t in thread PID: %ld\n", count, (long)pthread_self());
    // Do what you want to do here, and don't change anything else
    count++;
    
    while (!is_running2) {
      pthread_cond_signal(&cond2);
    }
      
    pthread_mutex_lock(&m1);
    is_running2 = 0;
    pthread_cond_wait(&cond1, &m1);
    is_running1 = 1;
    pthread_mutex_unlock(&m1);
  }

  pthread_mutex_lock(&m1);
  is_running = 1;
  pthread_mutex_unlock(&m1);
  while (!is_running2) {
      pthread_cond_signal(&cond2);
  }

  printf("first PID: %ld exit\n", (long)pthread_self());
  pthread_exit(NULL);
}

void *second(void *a)
{
  while (1) {
    pthread_mutex_lock(&m2);

    is_running1 = 0;
    pthread_cond_wait(&cond2, &m2);
  
    if (is_running) {
      is_running2 = 1;
      pthread_mutex_unlock(&m2);
      break;
    }

    is_running2 = 1;
    pthread_mutex_unlock(&m2);
    printf("count: %d\t in thread PID: %ld\n", count, (long)pthread_self());

    // Do what you want to do here, and don't change anything else
    count++;

    while (!is_running1) {
      pthread_cond_signal(&cond1);
    }
  }

  printf("second PID: %ld exit\n", (long)pthread_self());
  pthread_exit(NULL);
}

int main()
{
  pthread_t t1, t2;
  pthread_attr_t attr;

  pthread_attr_init(&attr);
  pthread_mutex_init(&m1, NULL);
  pthread_mutex_init(&m2, NULL);
  pthread_cond_init(&cond1, NULL);
  pthread_cond_init(&cond2, NULL);

  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  pthread_create(&t1, &attr, first, NULL);
  pthread_create(&t2, &attr, second, NULL);

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);

  pthread_attr_destroy(&attr);
  pthread_mutex_destroy(&m1);
  pthread_mutex_destroy(&m2);
  pthread_cond_destroy(&cond2);

  pthread_exit(NULL);

  return 0;
}
