/* WinThreadTemplate.cpp
 */

#if 1

/* Single-threaded collaboration template
 */
#include <iostream>
#include <windows.h>

using namespace std;
HANDLE g_go_1_hEvent;
HANDLE g_go_2_hEvent;

DWORD WINAPI MyThreadProc1(PVOID pParam);
DWORD WINAPI MyThreadProc2(PVOID pParam);

static volatile bool g_is_running = true;

int main()
{
	cout << "test WaitForSingleObject\n" << endl;

	g_go_1_hEvent = CreateEvent(NULL, TRUE, FALSE, TEXT("Test"));
	g_go_2_hEvent = CreateEvent(NULL, TRUE, FALSE, TEXT("Test"));

	HANDLE th1 = CreateThread(NULL, 0, MyThreadProc1, NULL, 0, NULL);
	HANDLE th2 = CreateThread(NULL, 0, MyThreadProc2, NULL, 0, NULL);

	WaitForSingleObject(th1, INFINITE);
	WaitForSingleObject(th2, INFINITE);

	system("pause");

	return 0;
}

DWORD WINAPI MyThreadProc1(PVOID pParam)
{
	UINT64 i = 0;
	DWORD dwReturn = 0;

	cout << "MyThreadProc 1 come in\n" << endl;

	while (1) {

		dwReturn = WaitForSingleObject(g_go_1_hEvent, INFINITE);

		if (!g_is_running) break;

		cout << "MyThreadProc 1 begin work " << i++ << endl;
		switch (dwReturn)
		{
		case WAIT_OBJECT_0:
			cout << "MyThreadProc 1 >>> WaitForSingleObject signaled\n" << endl;
			break;

		case WAIT_TIMEOUT:
			break;

		case WAIT_FAILED:
			break;
		}
		// notice No.2 thread go on
		Sleep(2000);
		SetEvent(g_go_2_hEvent);
		ResetEvent(g_go_2_hEvent);
	}

	cout << "MyThreadProc 1 leave" << endl;

	return 0;
}

DWORD WINAPI MyThreadProc2(PVOID pParam)
{
	UINT64 cnt = 10;
	cout << "MyThreadProc 2 come in\n" << endl;

	while (cnt--) {	
		cout << "MyThreadProc 2 >>> tell MyThreadProc 1 start \n" << endl;
		SetEvent(g_go_1_hEvent);
		ResetEvent(g_go_1_hEvent);
		cout << "MyThreadProc 2 >>> wait for MyThreadProc 1 end \n" << endl;
		WaitForSingleObject(g_go_2_hEvent, INFINITE);
	}
	
	cout << "MyThreadProc 2 leave\n" << endl;
	// Notice thread 1 to finish
	g_is_running = false;
	SetEvent(g_go_1_hEvent);
	ResetEvent(g_go_1_hEvent);

	return 0;
}

#endif

#if 0
/* Concurrent multithreading template
 */
#include <stdio.h>
#include <windows.h>

const unsigned int THREAD_NUM = 10;
DWORD WINAPI  ThreadFunc(LPVOID);

int main()
{
	printf("I am main thread�� thread id = %d\n", GetCurrentThreadId());
	HANDLE hThread[THREAD_NUM];
	for (int i = 0; i < THREAD_NUM; i++)
	{
		hThread[i] = CreateThread(NULL, 0, ThreadFunc, &i, 0, NULL); 
	}

	WaitForMultipleObjects(THREAD_NUM, hThread, true, INFINITE); // wait for sub threads finished

	system("pause");
	return 0;
}

DWORD WINAPI  ThreadFunc(LPVOID p)
{
	int n = *(int*)p;
	Sleep(1000*n); 
	printf("sub thread id = %d is working\t", GetCurrentThreadId()); 
	printf("sub thread id = %d exit\n\n", GetCurrentThreadId());

	return 0;
}

#endif