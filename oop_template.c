#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

struct object;
typedef int (*output_t)(struct object *this);
typedef void (*init_t)(struct object *this, void *data);
typedef void (*destroy_t)(struct object *this);
typedef struct object {
    char data[32];
    uint32_t length;
    init_t init;
    output_t out;
    destroy_t destroy;
} object_t;

void my_init(object_t *this, void *data)
{
    this->length = 32;
    memset(this->data, 0, this->length);
    sprintf(this->data, "%s", (char*)(data));
}

void my_destroy(object_t *this)
{
    ;
}

int my_output(object_t *this)
{
    printf("%s\n", this->data);
    return 0;
}

int main()
{
    object_t obj = {
        .init = my_init,
        .destroy = my_destroy,
        .out = my_output
    };

    obj.init(&obj, "Hello OOP World");
    obj.out(&obj);
    obj.destroy(&obj);
    return 0;
}
